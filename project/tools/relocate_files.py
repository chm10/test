from pathlib import Path
from shutil import copy
from re import compile
from project.tools import relocate_contets_to,delete_folder,rename_with_timestamp_place,insert_place,relocate_based_on_place
if __name__ == '__main__':
    src = Path('images/images2').resolve()
    dst = Path('images'.resolve())
    relocate_contets_to(src,dst)
    delete_folder('images/images2')

    ## Pattern YYYY-MM-DDTHH:MM:SS_place.jpg
    folder = Path('image').resolve
    original_name_regex = compile(r'([A-z])*[\d]?[\d]?_[\d*]\.jpg')
    rename_with_timestamp_place (folder,original_name_regex)

    new_regex = compile (r'[0-9]{8}_[0-9]{6}_([A-z]*)\.jpg')
    place = insert_place(folder,new_regex)
    relocate_based_on_place(folder,place)